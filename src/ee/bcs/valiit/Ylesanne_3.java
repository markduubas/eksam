package ee.bcs.valiit;
//        Kirjuta meetod, mille sisendparameetriks on Stringide massiiv ja
//        mis tagastab täisarvude massiivi. Tagastatava massiivi iga element
//        sisaldab endas vastava sisendparameetrina vastu võetud massiivi elemendi
//        (stringi) pikkust. Meetodi nime võid ise välja mõelda.
//        Kutsu see meetod main()-meetodist välja ja prindi tulemus
//        (kõik massiivi elemendid) standardväljundisse.


public class Ylesanne_3 {
    public static void main(String[] args) {
        String[] nimed = {"Mark", "Mihkel", "PeeterPeeretas", "Mati", "Joosep"};

            System.out.printf("Nimekirjas on %s nime.%n", nimed.length);

        for (int i = 0; i < nimed.length; i++) {

            System.out.printf("%s,", nimed[i]);
            System.out.printf(" %s tähte.%n", nimed[i].length());

        }

    }
}
