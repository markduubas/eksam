package ee.bcs.valiit;
//        Kirjuta meetod,mis võtab sisendparameetriks aastaarvu täisarvulisel kujul(int)vahemikus 1-2018
//        ja tagastab täisarvu(byte)vahemikus 1-21vastavalt sellele,mitmendasse sajandisse antud aasta kuulub.
//        Meetodi nime võid ise välja mõelda.Kui funktsioonile antakse ette parameeter,mis on kas suurem,
//        kui 2018või väiksem,kui 1,tuleb tagastada väärtus-1;
//         Kutsu see meetod välja main()-meetodist järgmiste erinevate sisendväärtustega:0,1,128,598,1624,1827,1996,2017.
//        Prindi need väärtused standardväljundisse.

import java.text.SimpleDateFormat;
import java.time.Year;
import java.util.Date;
import java.util.Scanner;

public class Ylesanne_4 {

    public static void main(String[] args) {

        Scanner sisse = new Scanner(System.in);
        int aasta, sajand;
        System.out.println("Tere, ütle yks aasta");
        aasta = sisse.nextInt();
        if (0 <= aasta && aasta <= 2018){
            sajand = aasta / 100 + 1;
        System.out.println("Sajand on:");
        System.out.println(sajand);}

        else
            System.out.println("-1");

    }
}
