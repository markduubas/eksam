package ee.bcs.valiit;
//        Defineeri klass Country, millel oleksid meetodid getPopulation, setPopulation, getName, setName ja list
//        riigis enim kõneldavate keeltega.
//        Override’i selle klassi toString() meetod nii, et selle meetodi väljakutsumine tagastaks teksti,
//        mis sisaldaks endas kõiki parameetreid väljaprindituna.
//        Tekita antud klassist üks objekt ühe vabalt valitud riigi andmetega
//        ja prindi selle riigi info välja .toString() meetodi abil.

import java.util.ArrayList;
import java.util.List;

public class Country {

    public String name = "";
    public double population = 0;
    static List<Country> keeled = new ArrayList<>(3);

    public Country(String name, double population, ArrayList keeled){
        this.name=name;
        this.population=population;
        this.keeled=keeled;
    }
    public double getPopulation() {
        return population;
    }

    public String getName() {
        return name;
    }

}
