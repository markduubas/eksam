//package ee.bcs.valiit;
////        Defineeri klass Country, millel oleksid meetodid getPopulation, setPopulation, getName, setName ja list
////        riigis enim kõneldavate keeltega.
////        Override’i selle klassi toString() meetod nii, et selle meetodi väljakutsumine tagastaks teksti,
////        mis sisaldaks endas kõiki parameetreid väljaprindituna.
////        Tekita antud klassist üks objekt ühe vabalt valitud riigi andmetega
////        ja prindi selle riigi info välja .toString() meetodi abil.
//
//
//public class Ylesanne_5 {
//    public static void main(String[] args) {
//        Country Eesti = new Country("Eesti", 1400000, "Eesti", "Vene", "Soome");
//        Country Soome = new Country("Soome", 4000000, "Soome", "Rootsi", "Norra");
//
//    }
//
//    public static String getName() {
//        return String.format("Riigi nimi on %s", getName());
//
//
//    }
//
//    public static String getPopulation() {
//        return String.format("Rahvaarv on: %s", getPopulation());
//    }
//
//
//}
